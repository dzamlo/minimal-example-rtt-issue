#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use core::panic::PanicInfo;

use defmt::{error, info};
use defmt_rtt as _;
use embassy_executor::Spawner;
use embassy_time::Timer;
use hal::clock::ClockControl;
use hal::embassy;
use hal::gpio::IO;
use hal::peripherals::Peripherals;
use hal::prelude::*;
use hal::systimer::SystemTimer;

#[main]
async fn main(_spawner: Spawner) {
    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::boot_defaults(system.clock_control).freeze();

    embassy::init(&clocks, SystemTimer::new(peripherals.SYSTIMER));

    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
    let mut led = io.pins.gpio7.into_push_pull_output();
    loop {
        led.toggle().unwrap();
        info!("hello...");
        Timer::after_millis(500).await;
    }
}

#[panic_handler]
fn panic(panic_info: &PanicInfo) -> ! {
    if let Some(location) = panic_info.location() {
        error!(
            "Panic occurred in file '{}' at line {}",
            location.file(),
            location.line(),
        );
    } else {
        error!("Panic occurred at unknown location");
    }
    loop {}
}
